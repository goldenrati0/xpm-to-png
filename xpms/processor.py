IGNORE_LINES = list(range(1, 9))


def process_term_xpm():
    line_number = 1
    xpm_lines = []
    with open("/home/tahmid/workspace/python/xpm-to-png/xpms/terminator.xpm", "r") as xpm:
        for line in xpm:
            if line_number not in IGNORE_LINES:
                if line[-2] == ",":
                    xpm_lines.append(line[1:-3])
                elif line[-2] == ";":
                    xpm_lines.append(line[1:-4])
            line_number += 1
    return xpm_lines


if __name__ == '__main__':
    print(process_term_xpm())
